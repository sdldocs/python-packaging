## Python Packing 살펴보기 <a id="get-to-know"></a>
Python의 packaging은 초보 개발자는 물론 베테랑 개발자 모두에게 복잡하고 혼란스러워 보일 수 있다. 인터넷을 통해 상충되는 조언을 발견할 수도 있으며, 한때 좋은 관행으로 여겨졌던 것이 이제는 눈살을 찌푸리게 만들지도 모른다.

이러한 상황의 주된 이유는 Python이 상당히 오래된 프로그래밍 언어이기 때문이다. 실제로 Python의 첫 번째 버전은 [World Wide Web](https://en.wikipedia.org/wiki/History_of_the_World_Wide_Web)이 일반 대중에게 공개되기 전인 1991년에 출시되었다. 물론, package 배포를 위한 최신 웹 기반 시스템은 초기 버전의 Python에는 포함되지 않았었고 심지어 계획조차 없었다.

대신 Python의 packaging 생태계는 사용자 요구가 명확해지고 기술이 새로운 가능성을 제시하면서 수십 년 동안 유기적으로 진화해 왔다. 최초의 packaing 지원은 2000년 가을에 시작되었으며 배포판 라이브러리는 Python 1.6과 2.0에 포함되었다. Python Packaㅎing Index(PyPI)는 2003년에 온라인으로 제공되었으며, 이는 원래 기존 package의 순수 인덱스로서 호스팅 기능이 없었다.

> **Note**: PyPI는 종종 Monty Python의 유명한 [치즈 샵](https://montypython.fandom.com/wiki/Cheese_Shop_sketch) 스케치를 언급하며 **Python 치즈 샵**으로 언급된다. 오늘날까지, [cheeseshop.python.org](http://cheeseshop.python.org/)는 PyPI로 리디렉션된다.

지난 10년 동안 많은 이니셔티브가 packaging 환경을 개선하여 Wild West에서 상당히 현대적이고 능력 있는 시스템으로 만들어졌다. 이는 주로 [Pypon Packaging Authority](https://www.pypa.io/)(PyPA) working group(워킹 그룹)이 검토하고 구현하는 Pypon Enhancement Proposals(PEPs)를 통해 이루어 진다.

Python packaging의 작업 방식을 정의하는 중요한 문서는 아래와 같다.

- [PEP 427](https://peps.python.org/pep-0427/)은 **wheel**을 packaing하는 방법을 설명
- [PEP 440](https://peps.python.org/pep-0440/)은 **버전 번호**를 구문 분석하는 방법을 설명
- [PEP 508](https://peps.python.org/pep-0508/)은 **종속성(dependancy)**을 명시하는 방법을 설명
- [PEP 517](https://peps.python.org/pep-0517/)은 **빌드 백엔드**의 작동 방식을 설명
- [PEP 518](https://peps.python.org/pep-0518/)은 **빌드 시스템**을 지정하는 방법을 설명
- [PEP 621](https://peps.python.org/pep-0621/)은 **프로젝트 메타데이터**를 작성하는 방법을 설명
- [PEP 660](https://peps.python.org/pep-0660/)은 **편집 가능한 설치**를 수행하는 방법을 설명

이 기술 문서를 공부할 필요가 없다. 이 페이지에서는 package를 퍼블리시하는 과정을 거치면서 이러한 모든 사양이 실제로 어떻게 결합되는지 보게 된다.

PyCon UK 2019에서 발표한 Thomas Kluyver의 프레젠테이션 "[Python packaging: How did we get here, and where are we going?](https://pyvideo.org/pycon-uk-2019/what-does-pep-517-mean-for-packaging.html)"에서 Python packaging의 간략한 내력을 알 수 있다. 또한 [PyPA](https://www.pypa.io/en/latest/presentations/) 웹 사이트에 더 많은 프레젠테이션을 찾아 볼 수 있다.

## 작은 Python Package 생성 <a id="cretae-small-python-package"></a>
이 절에서는 PyPI에 퍼믈리시시할 수 있는 예로 사용할 수 있는 작은 Python package에 대해 설명한다. 퍼블리시하려는 package가 이미 있는 경우 이 절을 건너뛰고 [다음 절](#prepare-package-publication)부터 시작할 수 있다.

여기서 다루는 package를 reader라 한다. 코드로 Real Python 튜토리얼을 다운로드하기 위한 라이브러리와 콘솔에서 튜토리얼을 읽기 위한 어플리케이션으로 모두 사용할 수 있다.

> **Note**: 이 절에서 설명하고 있는 Real Python feed reader 소스 코드는 단순하지만 완전히 기능하는 버전이다. 현재 [PyPI](https://pypi.org/project/realpython-reader/)에 퍼블리시된 버전과 비교하면 이 버전은 일부 오류 처리 및 추가 옵션이 없다.

먼저, reader의 디렉토리 구조를 살펴보자. package는 어떤 이름으로도 지정할 수 있는 디렉토리 안에 있다. 이 경우, <code>realpython-reader/</code>라고 하자. 소스 코드는 <code>src/</code> 디렉토리 안에 있다. 꼭 필요하지 않지만 대개는 [좋은 생각](https://packaging.python.org/en/latest/tutorials/packaging-projects/)이다.

> **Note**: package를 구조화할 때 추가 <code>src/</code> 디렉토리를 사용하는 것은 수년간 Python 커뮤니티에서 논의되어 왔다. 일반적으로 평평한 디렉토리 구조는 시작하기에 약간 더 쉽지만, <code>src/</code>-구조는 프로젝트의 성장에 따라 몇 가지 [이점](https://hynek.me/articles/testing-packaging/)을 제공한다.

<code>src/reader/</code> 디렉토리 내부에 모든 소스 코드가 포함되어 있다.

```
realpython-reader/
│
├── src/
│   └── reader/
│       ├── __init__.py
│       ├── __main__.py
│       ├── config.toml
│       ├── feed.py
│       └── viewer.py
│
├── tests/
│   ├── test_feed.py
│   └── test_viewer.py
│
├── LICENSE
├── MANIFEST.in
├── README.md
└── pyproject.toml
```

package의 소스 코드는 구성 파일과 같이 <code>src/</code> 하위 디렉터리에 있다. 별도의 하위 디렉터리 <code>tests/</code>에 몇 가지 테스트 프로그램가 있다. 테스트 자체는 이 튜토리얼에서 다루지 않지만 [나중에](#test-package) tests 디렉터리를 처리하는 방법에 대해 설명한다. [Python에서의 테스트 시작](https://realpython.com/python-testing/) 및 [Pytest를 통한 효과적인 Python 테스트](https://realpython.com/pytest-python-testing/)에서 일반적인 테스트에 대한 자세히 설명하고 있다.

자신의 package로 작업하는 경우 다른 구조를 사용하거나 package 디렉터리에 다른 파일이 있을 수 있다. [Python Application Layouts](https://realpython.com/python-application-layouts/)에서 몇 가지 다른 옵션에 대해 설명하고 있다. PyPI에 퍼블리시하는 아래 단계에서 사용하는 레이아웃과는 독립적으로 작동한다.

이 절의 나머지 부분에서 reader package의 작동 방법을 설명하고 있다. [다음 절](#prepare-package-publication)에서는 package를 퍼블리싱하는 데 필요한 <code>LICENSE</code>, <code>MANIFEST.in</code>, <code>README.md</code> 및 <code>pyproject.toml</code>과 같은 특수 파일에 대해 자세히 설명한다.

### Real Python Reader 사용
reader는 [Real Python feed](https://realpython.com/contact/#rss-atom-feed)에서 최신 Real Python 튜토리얼을 다운로드할 수 있는 기본 [web feed](https://en.wikipedia.org/wiki/Web_feed) reader이다.

이 절에서는 먼저 reader로부터 기대하는 몇 가지 출력 예를 볼 수 있다. 아직 직접 이러한 예제를 실행할 수는 없지만, 도구가 어떻게 동작하는 지는 알 수 있을 것이다.

> **Note**: reader의 소스 코드를 다운로드한 경우 먼저 가상 환경을 생성한 다음, 해당 가상 환경 내에 로컬로 package를 설치하는 방법으로 아래와 같이 수행할 수 있다.
>
```shell
(venv) $ python -m pip install -e .
```

> 이 페이지에서 위의 명령을 실행할 때 후드 아래에서 수행되는 동작에 대해 자세히 설명한다.

첫 번째 예제에서 reader를 사용하여 최신 기사 목록을 가져온다.

```shell
$ python -m reader
The latest tutorials from Real Python (https://realpython.com/)
  0 How to Publish an Open-Source Python Package to PyPI
  1 The Real Python Podcast – Episode #110
  2 Build a URL Shortener With FastAPI and Python
  3 Using Python Class Constructors
  4 Linear Regression in Python
  5 The Real Python Podcast – Episode #109
  6 pandas GroupBy: Your Guide to Grouping Data in Python
  7 Deploying a Flask Application Using Heroku
  8 Python News: What's New From April 2022
  9 The Real Python Podcast – Episode #108
 10 Top Python Game Engines
 11 Testing Your Code With pytest
 12 Python's min() and max(): Find Smallest and Largest Values
 13 Real Python at PyCon US 2022
 14 Why Is It Important to Close Files in Python?
 15 Combining Data in Pandas With merge(), .join(), and concat()
 16 The Real Python Podcast – Episode #107
 17 Python 3.11 Preview: Task and Exception Groups
 18 Building a Django User Management System
 19 How to Get the Most Out of PyCon US
```

이 리스트는 최신 튜토리얼을 보여주므로 위와는 다를 수 있다. 그래도 각 아티클(article)에는 번호가 매겨져 있다. 특정 튜토리얼을 읽으려면 동일한 명령을 사용하지만 튜토리얼의 번호도 포함하여야 한다.

> **Note**: Real Python feed는 기사 미리 보기를 포함하고 있다. 따라서 reader로 튜토리얼 전체를 읽을 수 없다.

이 경우 [How to Publish an Open-Source Python Package to PyPI](https://realpython.com/pypi-publish-python-package/)를 읽으려면 명령에 0을 추가한다.

```shell
$ python -m reader 0
How to Publish an Open-Source Python Package to PyPI

Python is famous for coming with batteries included, and many sophisticated
capabilities are available in the standard library. However, to unlock the
full potential of the language, you should also take advantage of the
community contributions at PyPI: the Python Packaging Index.

PyPI, typically pronounced pie-pee-eye, is a repository containing several
hundred thousand packages. These range from trivial Hello, World
implementations to advanced deep learning libraries. In this tutorial,
you'll learn how to upload your own package to PyPI. Publishing your
project is easier than it used to be. Yet, there are still a few
steps involved.

[...]
```

위 명령의 결과는 [Markdown](https://www.markdownguide.org/basic-syntax) 형식으로 문서를 콘솔에 인쇄한다.

> **Note**: [module](https://realpython.com/python-import/#modules) 또는 [package](https://realpython.com/python-import/#packages)를 실행하는 데 <code>python -m</code>을 [사용](https://docs.python.org/3/using/cmdline.html#cmdoption-m)할 수 있다. 모듈 및 일반 스크립트용 Python과 유사하게 동작한다. 예를 들어 <code>python module.py</code>와 <code>python -m module</code>은 거의 동일하다.

> <code>-m</code>으로 package를 실행하면 package 내의 파일 <code>__main.__py</code>가 실행된다. 자세한 내용은 [reader 호출](#call-the-reader)를 참조하시오.

> 지금 <code>src/</code> 디렉토리 내부에서 <code>python-m reader</code> 명령을 실행해야 합니다. 나중에 워킹 디렉토리에서 명령을 실행하는 방법에 대해 설명한다.

명령에서 번호를 변경하여 읽기 가능한 튜토리얼를 읽을 수 있다.

### Reader 코드 이해
이 튜토리얼에서는 판독기 작동 방법에 대한 세부 정보는 중요하지 않다. package는 5개의 파일로 구성되어 있다. 따라서 일단 건너 뛴다.

### Reader 호출 <a id="call-the-reader"></a>
프로젝트가 복잡해질 때 한 가지 과제는 사용자에게 프로젝트를 어떻게 사용할 수 있는지 설명하는 것이다. reader는 네 개의 다른 소스 코드 파일로 구성되기 때문에, 사용자는 어플리케이션을 사용하기 위해 어떤 파일을 실행해야 하는지 어떻게 알 수 있을까?

> **Note**: 일반적으로 단일 Python 파일을 **스크립트(script)** 또는 [module](https://realpython.com/python-import/#modules)이라고 한다. [package](https://realpython.com/python-import/#packages)를 module의 집합으로 생각할 수 있다.

일반적으로 파일 이름을 제공하여 Python 스크립트를 실행한다. 예를 들어 <code>hello.py</code>라는 스크립트가 있는 경우 다음과 같이 실행할 수 있다.

```shell
$ python hello.py
Hi there!
```

이 스크립트를 실행하면 <code>Hi There!</code>를 콘솔에 출력한다. 마찬가지로 Python 인터프리터에 <code>-m</code> 옵션을 사용하여 파일 이름 대신 module 이름을 지정하여 스크립트를 실행할 수 있다.

```shell
$ python -m hello
Hi there!
```

현재 디렉터리에 있는 모듈의 경우 <code>.py</code>-suffix르 생략한 파일 이름이 모듈 이름인 것이다.

<code>-m</code>을 사용하는 한 가지 장점은 Python에 내장된 module을 포함하여 [Python 경로](https://realpython.com/python-import/#pythons-import-path)에 있는 모든 module을 호출할 수 있는 점이다. 한 가지 예는 [antigravity](http://python-history.blogspot.com/2010/06/import-antigravity.html)을 부르는 것이다.

```shell
$ python -m antigravity
Created new window in existing browser session.
```

<code>-m</code> 없이 built-in module을 실행하려면 먼저 시스템에서 module이 저장된 위치를 찾아보고 전체 경로로 호출해야 한다.

module뿐만 아니라 package에서도 동작한다는 점도 <code>-m</code>을 사용하는 장점이다. 앞에서 설명한 것처럼, 워킹 디렉터리에서 <code>reader/</code> 디렉토리를 사용할 수 있다면 <code>-m</code>로 reader package를 호출할 수 있다.

```shell
$ cd src/
$ python -m reader
```

reader가 package이기 때문에 이름은 디렉토리만 참조한다. Python은 해당 디렉터리 내에서 실행할 코드를 어떻게 결정할까? <code>__main__.py</code>라는 파일을 찾는다. 파일이 있으면 실행한다. 존재하지 않으면 다음과 같은 오류 메시지를 출력한다.

```shell
$ python -m urllib
python: No module named urllib.__main__; 'urllib' is a package and
        cannot be directly executed
```

오류 메시지는 표준 라이브러리의 [urllib package](https://realpython.com/urllib-request/)에 <code>__main__.py</code> 파일이 없음을 나타낸다.

실행되어야 할 package를 생성하는 경우 <code>__main__.py</code> 파일을 포함해야 한다. 또한 <code>python-m rich</code>를 사용하여 package의 기능을 [시연하는](https://pypi.org/project/rich/) Rich의 훌륭한 예를 따를 수 있다.

[이후](https://realpython.com/pypi-publish-python-package/#configure-package) 일반 명령어 프로그램처럼 동작하는 package의 시작점(entry-point)을 만드는 방법도 볼 수 있다. 이러한 기능은 최종 사용자가 더욱 쉽게 사용할 수 있다.

## package 퍼블리싱 준비 <a id="prepare-package-publication"></a>
퍼블리시하려는 package가 있다. reader를 복사했거나 개발한 package가 있을 수 있다. 이 절에서는 PyPI에 package를 업로드하기 전에 수행해야 하는 단계를 설명하고 확인할 수 있다.

### package 명명
첫 번째, 그리고 아마도 [가장 어려운](https://devrant.com/rants/2004238/variable-name) 단계는 package에 대한 좋은 이름을 찾는 것이다. PyPI의 모든 package는 고유한 이름을 갖어야 한다. 현재 PyPI에는 수십만 개의 package가 있으므로, 가장 적합한 이름은 이미 사용되었을 가능성이 있다.

대표적인 예로, PyPI에는 [이미](https://pypi.org/project/reader/) reader라는 이름의 package가 있다. package 이름을 고유하게 만드는 한 가지 방법은 이름에 인식 가능한 접두사를 추가하는 것이다. 예에서는 <code>realpython-reader</code>를 reader package의 PyPI 이름으로 사용한다.

package에 대해 선택한 PyPI 이름은 <code>pip</code>과 함께 설치할 때 사용된다.

```shell
$ python -m pip install realpython-reader
```

PyPI 이름은 package 이름과 일치할 필요는 없다. 여기서 package 이름은 여전히 reader이며 package를 import할 때 사용해야 하는 이름이다.

```python
>>> import reader
>>> reader.__version__
'1.0.0'

>>> from reader import feed
>>> feed.get_titles()
['How to Publish an Open-Source Python Package to PyPI', ...]
```

때때로 package에 다른 이름을 사용해야 한다. 그러나 package 이름과 PyPI 이름이 같으면 사용자의 작업을 단순하게 유지할 수 있다.

package 이름은 PyPI 이름처럼 전체적으로 고유할 필요는 없지만 실행 중인 환경에서 고유해야 한다.

동일한 package 이름 예를 들면 reader와 realpython-reader 두 package를 설치하는 경우 <code>import reader</code>와 같은 문장은 모호해 진다. Python은 import 경로에서 가장 먼저 찾은 package를 import하여 이 문제를 해결한다. 이름을 알파벳 순으로 정렬할 때 reader package가 첫 번째 package가 되는 경우가 많다. 하지만, 이같은 해결방안에 의존하지 말아야 한다.

일반적으로 package 이름을 가능한 고유하게 유지하면서 짧고 간결한 이름으로 편리함과 균형을 맞추기를 원할 것이다. 실제 <code>realpython-reader</code>는 특수 feed reader인 반면, PyPI reader는 더 일반적이다. 이 페이지의 목적상, 두 가지 모두를 필요로 할 이유가 없으므로 고유하지 않은 이름에 대한 문제를 염두에 둘 필요가 없다.

### package 구성 <a id="configure-package"></a>
PyPI에 퍼블시리할 package를 준비하려면 해당 package에 대한 정보를 제공해야 한다. 일반적으로 다음 두 가지 유형의 정보를 명시해야 한다.

1. 빌드 시스템의 구성
1. package 구성

**빌드 시스템**은 일반적으로 [wheel](https://realpython.com/python-wheels/) 또는 [소스 배포(sdist)](https://packaging.python.org/en/latest/specifications/source-distribution-format/) 형식으로 PyPI에 업로드할 실제 파일을 생성한다. 오랫동안, 이를 위하여 [disutil](https://docs.python.org/3.11/library/distutils.html)이나 [setuptools()](https://setuptools.pypa.io/)를 사용하였다. 그러나 [PEP 517](https://peps.python.org/pep-0517/)과 [PEP 518](https://peps.python.org/pep-0518/)에서 사용자 지정 빌드 시스템을 지정하는 방법을 도입했다.

> **Note**: 프로젝트에서 사용할 빌드 시스템을 선택할 수 있다. 빌드 시스템들의 주요 차이점은 package를 구성하는 방법과 packag 빌드와 업로드를 위해 실행하는 명령이다.

> 이 페이지에서 <code>setuptools</code>를 빌드 시스템으로 사용하는 것에 초점을 맞추고 있다. 그러나 [나중에](#other-build-systems) Flit과 Poetry같은 대체 시스템의 사용 방법을 설명할 것이다.

Python 프로젝트는 <code>pyproject.toml</code> 파일을 사용하여 빌드 시스템을 지정한다. 아래와 같은 <code>pyproject.toml</code>을 추가하여 <code>setuptools</code>을 사용할 수 있다.

```toml
# pyproject.toml

[build-system]
requires = ["setuptools>=61.0.0", "wheel"]
build-backend = "setuptools.build_meta"
```

이는 Python이 package를 빌드하기 위해 설치해야 하는 종속성뿐만 아니라 빌드 시스템으로 <code>setuptools</code>를 사용한다는 것을 나타내고 있다. 일반적으로 선택한 빌드 시스템의 [설명서](https://setuptools.pypa.io/en/latest/userguide/quickstart.html)에서 <code>pyproject.toml</code>에서 <code>build-system table</code>을 작성하는 방법을 설명한다.

제공해야 하는 더 흥미로운 정보는 package 자체와 관련이 있다. [PEP 621](https://peps.python.org/pep-0621/)은 package에 대한 메타데이터를 다양한 빌드 시스템에서 가능한 한 통일된 방식으로 <code>pyproject.toml</code>에 포함시키는 방법을 정의한다. 

> **Note**: 지금까지 setuptools는 [setup.py](https://setuptools.pypa.io/en/latest/userguide/quickstart.html#setup-py)을 사용하여 package를 구성했다. 이것이 설치 시 실행되는 실제 Python 스크립트이기 때문에 매우 강력하며 복잡한 package를 빌드할 때 여전히 필요할 수 있다.

> 그러나 일반적으로 선언형 구성 파일을 사용하여 package를 빌드하는 방법을 표현하는 것이 더 좋습니다. 이 파일은 추론하기가 더 쉽고 걱정할 함정이 더 적기 때문이다. [setup.cfg](https://setuptools.pypa.io/en/latest/userguide/declarative_config.html)를 이용하는 것이 setuptools를 구성하는 가장 일반적인 방법이다.

> 그러나 setuptools은 PEP 621에 지정된 대로 [pyproject.toml](https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html)을 사용하는 방향으로 이동하고 있다. 이 페이지에서는 모든 package 구성에 <code>pyproject.toml</code>을 사용한다.

reader package의 최소 구성은 다음과 같을 수 있다.

```toml
# pyproject.toml

[build-system]
requires      = ["setuptools>=61.0.0", "wheel"]
build-backend = "setuptools.build_meta"

[project]
name = "realpython-reader"
version = "1.0.0"
description = "Read the latest Real Python tutorials"
readme = "README.md"
authors = [{ name = "Real Python", email = "info@realpython.com" }]
license = { file = "LICENSE" }
classifiers = [
    "License :: OSI Approved :: MIT License",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
]
keywords = ["feed", "reader", "tutorial"]
dependencies = [
    "feedparser >= 5.2.0",
    "html2text",
    'tomli; python_version < "3.11"',
]
requires-python = ">=3.9"

[project.optional-dependencies]
dev = ["black", "bumpver", "isort", "pip-tools", "pytest"]

[project.urls]
Homepage = "https://github.com/realpython/reader"

[project.scripts]
realpython = "reader.__main__:main"
```

이 정보의 대부분은 선택 사항이며 예제에 포함되지 않은 다른 설정을 사용할 수 있다. 자세한 내용은 [문서](https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html)를 참조하시오.

<code>pyproject.toml</code>에 포함해야 하는 최소 정보는 다음과 같다.

- **name**은 PyPI에 표시될 package의 이름을 명시한다.
- **version**은 package의 현재 버전을 설정한다.

위의 예에서 알 수 있듯이 훨씬 더 많은 정보를 포함할 수 있다. <code>pyproject.toml</code>의 다른 키 중 일부를 다음과 같이 해석할 수 있다.

- **classifier**는 [classifier](https://pypi.org/classifiers/) 목록을 사용하여 프로젝트를 기술한다. 프로젝트를 더 쉽게 검색할 수 있으므로 이 옵션을 사용해야 한다.
- **dependency**은 package가 서드파티 라이브러리에 대해 가지는 종속성을 나열한다. reader는 <code>feed parser</code>, <code>html2text</code> 및 <code>tomli</code>에 의존적이므로 여기에 나열한다.
- **project.urls**은 사용자에게 package에 대한 추가 정보를 표시하는 데 사용할 수 있는 링크를 추가한다. 여기에 여러 링크들을 나열할 수 있다.
- **project.scripts**는 package 내에서 함수를 호출하는 명령줄 스크립트를 생성한다. 여기서 새로운 realpython 명령은 <code>reader.__main__</code> module 내의 <code>main()</code>을 호출한다.

<code>project.scripts</code> 테이블은 [시작점](https://packaging.python.org/en/latest/specifications/entry-points/)을 처리할 수 있는 세 테이블 중 하나이다. 또한 각각 GUI 어플리케이션과 [플러그인](https://packaging.python.org/en/latest/guides/creating-and-discovering-plugins/#using-package-metadata)을 지정하는 <code>project.gui-scripts</code>과 <code>project.entry-points</code>를 [포함할](https://peps.python.org/pep-0621/#entry-points) 수 있다.

이 모든 정보의 목적은 package를 PyPI에서 매력적이고 찾을 수 있도록 만드는 것이다. PyPI의 <code>realpython-reader</code> [프로젝트 페이지](https://pypi.org/project/realpython-reader/)를 보고 위의 <code>pyproject.toml</code> 정보와 비교하여 보시오.

![PyPI](pypi_realpython-reader.png)

PyPI에 대한 모든 정보는 <code>pyproject.toml</code>와 <code>README.md</code>에서 가져온다. 예를 들어 버전 번호는 <code>project.toml</code>의 라인 <code>version = "1.0.0"</code>을 기반으로 하며 *Read the latest Real Python tutorials*는 <code>description</code>에서 복사된다.

또한 <code>README.md</code> 파일로부터 project description을 복사한다. 사이드바에서 *Project Link* 섹션에서 <code>project.urls</code>과  *Meta* 섹션에서 <code>license</code>와 <code>authors</code>의 정보를 찾을 수 있다. 사이드바 맨 아래에서 <code>classifier</code>에서 지정한 값을 볼 수 있다

[PEP 621](https://peps.python.org/pep-0621/#specification)에서 모든 키에 대한 내용을 자세히 설명하고 있다. [다음](#package-dependencies)에서 종속성뿐만 아니라 <code>project.optional-dependencies</code>에 대해서도 자세히 살펴보도록 한다.


### package 종속성 지정 <a id="package-dependencies"></a>
package는 표준 라이브러리의 일부가 아닌 서드 파티 라이브러리에 종속될 수 있다. 이를 <code>pyproject.toml</code>의 종속성 목록에 명시해야 한다. 이를 위하여 다음과 같이 하여야 한다.

```toml
dependencies = [
    "feedparser >= 5.2.0",
    "html2text",
    'tomli; python_version < "3.11"',
]
```

이는 <code>reader</code>가 <code>feedparser</code>, <code>html2text</code> 및 <code>tomli</code>에 종속됨을 명시한다. 또한 다음과 같은 내용을 담고 있다.

- **feedparser****는 버전 5.2.0 이상이어야 한다.
- 모든 버전의 **html2text**를 허용한다.
- 모든 버전의 **tomli**가 가능하지만 Python 3.10 또는 그 이전 버전에서만 필요하다.

여기에는 [버전 지정자](https://peps.python.org/pep-0508/#specification)와 [환경 표시자](https://peps.python.org/pep-0508/#environment-markers)를 포함하여 종속성을 지정할 때 사용할 수 있는 몇 옵션을 표시한다. 후자를 사용하여 다양한 운영 체제, Python 버전 등을 설명할 수 있다.

그러나 라이브러리 또는 어플리케이션이 작동하는 데 필요한 최소 요구 사항만 지정해야 한다. 이 목록은 package가 설치될 때마다 <code>pip</code>에서 종속성을 해결하는 데 사용된다. 이 목록을 최소로 유지하면 package가 최대한으로 호환되도록 할 수 있다.

**의존관계를 정확히 지적해야** 한다는 것을 들어봤을 것이다. 정말 [좋은 조언](https://realpython.com/python-virtual-environments-a-primer/#pin-your-dependencies)이다! 그러나 이 경우에는 해당되지 않는다. 환경을 재현할 수 있도록 종속성을 고정한다. 반면, 당신의 package가 많은 다른 Python 환경에서 작동하기를 바란다.

package를 종속성에 추가할 때는 다음 [규칙](https://packaging.python.org/en/latest/discussions/install-requires-vs-requirements/)을 따라야 한다.

- 직접 종속성만 나열합니다. 예를 들면, <code>reader</code>는 <code>feedparser</code>, <code>html2text</code> 및 <code>tomli</code>를 import하므로 해당 항목을 나열한다. 반면에 <code>feedparser</code>는 [<code>sgmllib3k</code>](https://pypi.org/project/sgmllib3k/)에 의존하지만 <code>reader</code>는 이 라이브러리를 직접 사용하지 않기 때문에 지정하지 않는다.
- <code>==</code>로 특정 버전에 종속성을 고정하지 않는다.
- 종속성의 특정 버전에 추가된 기능에 의존하는 경우 <code>>=</code>을 사용하여 하한을 추가한다.
- 주 버전 업그레이드에서 종속성으로 인해 호환성이 손상될 수 있는 경우 상한을 추가하려면 <code><</code>를 사용한다. 이 경우 이러한 업그레이드를 부지런히 테스트하여 가능하면 상한을 제거하거나 늘려야 한다.

이러한 규칙은 다른 사용자가 사용할 수 있도록 package를 구성할 때 유지되어야 한다. package를 배포하는 경우 [가상 환경](https://realpython.com/python-virtual-environments-a-primer/) 내에 종속성을 고정해야 한다.

[pip-tools](https://pypi.org/project/pip-tools/) 프로젝트는 고정된 종속성을 관리하는 좋은 도구이다. 전체 종속성 목록을 만들거나 업데이트할 수 있는 <code>pip-compile</code> 명령과 함께 제공된다.

예를 들어 <code>reader</code>를 가상 환경에 배포한다고 가정해 보자. 그러면, <code>pip-tools</code>를 사용하여 재현 가능한 환경을 생성할 수 있다. 실제로 <code>pip-compile</code>은 <code>pyproject.toml</code> 파일과 직접 연동할 수 있다.

```shell
(venv) $ python -m pip install pip-tools
(venv) $ pip-compile pyproject.toml
feedparser==6.0.8
    via realpython-reader (pyproject.toml)
html2text==2020.1.16
    via realpython-reader (pyproject.toml)
sgmllib3k==1.0.0
    via feedparser
tomli==2.0.1 ; python_version < "3.11"
    via realpython-reader (pyproject.toml)
```

<code>pip-compile</code>은 위의 출력과 유사한 내용인 세부 <code>requirements.txt</code> 파일을 생성한다. <code>pip install</code> 또는 <code>pip-sync</code>를 사용하여 환경에 다음과 같은 종속성을 설치할 수 있다.

```shell
(venv) $ pip-sync
Collecting feedparser==6.0.8
  ...
Installing collected packages: sgmllib3k, tomli, html2text, feedparser
Successfully installed feedparser-6.0.8 html2text-2020.1.16 sgmllib3k-1.0.0
                       tomli-2.0.1
```

[pip-tools 문서](https://pip-tools.readthedocs.io/)는 보다 자세한 정보를 제공한다. 

<code>project.optional-dependencies</code>라는 별도의 테이블에서 package의 선택적 종속성을 명시할 수도 있다. 종종 개발 또는 테스트 중에 사용할 종속성을 지정할 때 이 옵션을 사용한다. 그렇지만 package의 특정 기능을 지원하는 데 사용되는 추가 종속성을 지정할 수도 있다.

위의 예를 위하여 다음과 같이 작성한다.

```toml
[project.optional-dependencies]
dev = ["black", "bumpver", "isort", "pip-tools", "pytest"]
```

이렇게 하면 선택적 종속성의 한 그룹인 dev가 추가된다. 이러한 그룹을 여럿 가질 수 있으며 그룹의 이름을 지정할 수 있다.

기본적으로 선택적 종속성은 package를 설치할 때 포함되지 않는다. 그러나 <code>pip</code>를 실행할 때 대괄호 안에 그룹 이름을 추가하여 그룹 이름을 수동으로 설치하도록 지정할 수 있다. 예를 들어 다음을 수행하여 <code>reader</code>의 추가 개발 종속성을 설치할 수 있다.

```shell
(venv) $ python -m pip install realpython-reader[dev]
```

<code>pip-compile</code>로 종속성을 고정할 때 <code>--extra</code> 명령 옵션을 사용하여 선택적 종속성을 포함할 수도 있다.

```shell
(venv) $ pip-compile --extra dev pyproject.toml
attrs==21.4.0
    via pytest
black==22.3.0
    via realpython-reader (pyproject.toml)
...
tomli==2.0.1 ; python_version < "3.11"
    via
      black
      pytest
      realpython-reader (pyproject.toml)
```

이렇게 하면 일반 종속성과 개발 종속성을 모두 포함하는 고정된 <code>requirements.txt</code> 파일을 생성한다. 

### package 문서화
package를 세상에 공개하기 전에 [문서](https://realpython.com/documenting-python-code/)를 추가해야 한다. 프로젝트에 따라 문서는 단일 README 파일만큼 작거나 튜토리얼, 예제 갤러리 및 API 참조를 포함하는 웹 페이지만큼 포괄적일 수 있다.

최소한 프로젝트에 README 파일을 포함해야 한다. 잘 작성된 [README](https://readme.so/)은 package를 설치하고 사용하는 방법을 설명할 뿐만 아니라 프로젝트 또한 빠르게 설명해야 한다. <code>pyproject.toml</code>의 <code>readme</code> 키에서 README를 참조하려는 경우가 많이 있다. 그러면 PyPI 프로젝트 페이지에도 정보가 표시된다.

프로젝트 설명의 형식으로서의 텍스트로 [Markdown](https://www.markdownguide.org/basic-syntax) 또는 [resStructuredText](http://docutils.sourceforge.net/rst.html)를 사용할 수 있다. PyPI는 파일 확장명을 기준으로 어떤 형식을 사용하고 있는지 파악한다. resStructuredText의 고급 기능이 필요하지 않다면 README에 Markdown을 사용하는 것을 추천한다. 기능이 더 단순하고 PyPI 이외 지원 범위가 넓다.

큰 프로젝트의 경우 단일 파일로 문서를 제공하기 보다 여러 파일로 더 많은 문서를 제공할 수 있다. 이 경우 [GitHub](https://github.com/) 또는 [Read the Docs](https://readthedocs.org/)와 같은 사이트에서 문서를 호스팅하고 PyPI 프로젝트 페이지에서 문서에 연결할 수 있다.

<code>pyproject.toml</code>의 <code>project.urls</code> 테이블에 다른 URL을 지정하여 연결할 수 있다. 이 예에서 <code>reader</code> GitHub 저장소에 연결하는 데 URLs 섹션을 사용한다.

### package 검사 <a id="test-package"></a>
테스트는 package를 개발할 때 유용하며, 테스트를 포함해야 한다. 이 페이지에서는 테스트를 다루지 않지만 <code>test/</code>소스 코드 디렉토리에서 reader의 테스트를 볼 수 있다.

[Exffective Python Testing with PyTest](https://realpython.com/pytest-python-testing/)에서 테스트에 대해 자세히 알아보고, [Build a Hash Table in Python With TDD](https://realpython.com/python-hash-table/)와 [Python Practice Problems: Parsing CSV Files](https://realpython.com/python-interview-problem-parsing-csv-files/)에서 테스트 기반 개발(TDD)에 대한 실제 체험을 할 수 있다.

퍼블리싱를 위해 package를 준비할 때 테스트가 수행하는 역할을 인지해야 한다. 일반적으로 개발자만을 위한 것이므로, PyPI를 통해 배포하는 package에 포함되어서는 안 된다.

최신 버전의 설치 도구는 [코드 검색](https://setuptools.pypa.io/en/latest/userguide/package_discovery.html#automatic-discovery)을 매우 잘하며 일반적으로 package 배포에 소스 코드를 포함하지만 테스트, 문서 및 유사한 개발 아티팩트는 제외한다.

<code>pyproject.toml</code>에서 지시문을 찾아 package에 포함된 내용을 정확하게 제어할 수 있다. 자세한 내용은 [Setuptools 문서](https://setuptools.pypa.io/en/latest/userguide/package_discovery.html#custom-discovery)를 참조하시오.

### package 버전 
package에는 버전이 표시 되어어야 한다. 또한 PyPI는 특정 버전의 package를 한 번만 업로드할 수 있도록 하고 있다. 즉, PyPI에서 package를 업데이트하려면 먼저 버전 번호를 늘려야 한다. 이는 재현성을 보장하는 데 도움이 되기 때문에 바람직 한 것이다. 주어진 package의 버전이 동일한 두 환경에서 동일하게 동작해야 한다.

사용할 수 있는 다양한 [버전 스킴(scheme)](https://en.wikipedia.org/wiki/Software_versioning)이 있다. Python 프로젝트의 경우 [PEP 440](https://peps.python.org/pep-0440/)은 몇 가지 방법을 권장하고 있다. 그러나 유연성을 갖기 위해 PEP에 대한 설명은 너무 복잡하다. 간단한 프로젝트의 경우 간단한 버전 관리 체계를 사용해야 한다.

[Semantic versioning](https://semver.org/)는 비록 [완벽하지 않더라도](https://hynek.me/articles/semver-will-not-save-you/) 사용하기 좋은 디폴트 체계이다.  일례로 1.2.3을 위해 세개의 숫자 요소들로 버전을 지정한다. 그 구성 요소는 각각 MAJOR, MINOR 및 PATCH라 불린다. 각 구성 요소 증가시킬 것에 대해 다음과 권고하고 있다.

- compatible하지 않은 API 변화가 있다면 MAJOR 버전을 증가시킨다.
- 이전 버전과 호환이 되는 방식으로 기능을 추가하는 경우 MINOR 버전을 증가시킨다.
- 버그 수정으로 이전 버전과 호환이 되는 경우 PATCH 버전을 증가시킨다. ([출처](https://semver.org/#summary))

MINOR 버전을 증가시키면 패치 버전을 0으로 재설정하고 MAJOR 버전을 증가시키면 패치와 MINOR 모두를 0으로 재설정해야 한다.

[Calendar versioning](https://calver.org/)은 인기를 얻고 있는 시맨틱 버전의 대안이며 [Ubuntu](http://www.ubuntu.com/), [Twisted](https://twistedmatrix.com/), [Black](https://black.readthedocs.io/) 및 [pip](https://pip.pypa.io/)과 같은 프로젝트에서 사용된다. 캘린더 버전도 여러 숫자 구성 요소로 구성되지만, 이 중 하나 또는 여럿이 현재 연도, 월 또는 주에 연결되어 있다.

프로젝트 내의 다른 파일에 버전 번호를 지정하려는 경우가 많이 있다. 예를 들어 버전 번호는 <code>pyproject.toml</code>과 reader package의 <code>reader/__init__.py</code>에 모두 언급되어 있다. 버전 번호의 일관성을 유지하기 위해 [BumpVer](https://pypi.org/project/bumpver/)와 같은 도구를 사용할 수 있다.

BumpVer를 사용하면 파일에 직접 버전 번호를 기록한 다음 필요에 따라 업데이트할 수 있다. 예를 들어 다음과 같이 BumpVer를 프로젝트에 설치하고 통합할 수 있다.

```shell
(venv) $ python -m pip install bumpver
(venv) $ bumpver init
WARNING - Couldn't parse pyproject.toml: Missing version_pattern
Updated pyproject.toml
```

<code>bumpver init</code> 명령으로 프로젝트를 위한 도구를 구성할 수 있는 <code>pyproject.toml</code>에 섹션을 생성한다. 필요에 따라 많은 디폴트 설정을 변경해야 할 수 있다. reader의 경우 다음과 같은 결과가 나타날 수 있다.

```toml
[tool.bumpver]
current_version = "1.0.0"
version_pattern = "MAJOR.MINOR.PATCH"
commit_message  = "Bump version {old_version} -> {new_version}"
commit          = true
tag             = true
push            = false

[tool.bumpver.file_patterns]
"pyproject.toml" = ['current_version = "{version}"', 'version = "{version}"']
"src/reader/__init__.py" = ["{version}"]
```

BumpVer가 바르게 작동하려면 <code>file_patterns</code> 하위 섹션에 버전 번호가 포함된 모든 파일을 지정해야 한다. BumpVer는 Git과 잘 연동하며 버전 번호를 업데이트할 때 자동으로 commit, tag 및 push할 수 있다.

> **Note**: BumpVer는 [버전 관리 시스템](https://realpython.com/python-git-github-intro/)과 연동할 수 있다. repository에 commit되지 않은 변경 사항이 있는 경우 파일 업데이트가 거부된다.

구성을 설정한 다음, 명령어 하나로 모든 파일의 버전을 범핑할 수 있다. 예를 들어, reader의  MINOR 버전을 늘리려면 다음같이 수행한다.

```shell
(venv) $ bumpver update --minor
INFO    - Old Version: 1.0.0
INFO    - New Version: 1.1.0
```

그러면 <code>pyproject.toml</code>과 <code>__init__.py</code>의 버전 번호가 1.0.0에서 1.1.0으로 변경된다. <code>--dry</code> 플래그를 사용하면 실제로 실행하지 않고도 BumpVer가 어떤 변경을 수행할지 확인할 수 있다.

### package에 리소스 파일 추가
package 안에 소스 코드 파일이 아닌 파일이 있을 수 있다. 예를 들어 데이터 파일, 이진 파일, 문서 및 (이 예에서와 같이) 구성 파일 등이다.

프로젝트를 빌드할 때 이러한 파일이 포함되도록 하려면 매니페스트 파일을 사용한다. 대부분의 프로젝트에서 매니페스트에 대하여 걱정할 필요가 없다. 기본적으로 Setuptools는 빌드에 모든 소스 코드 파일과 README 파일을 포함시킨다.

다른 리소스 파일이 있고 매니페스트를 업데이트해야 하는 경우 프로젝트의 기본 디렉터리에 있는 <code>pyproject.toml</code> 옆에 <code>MANIFEST.in</code>이라는 파일을 생성해야 한다. 이 파일은 포함할 파일과 제외할 파일에 대한 규칙을 지정한다.

```text
# MANIFEST.in

include src/reader/*.toml
```

위의 예에서는 <code>src/reader/</code> 디렉토리에 있는 모든 <code>.toml</code> 파일을 포함한다. 실제로 이 파일은 구성 파일이다.

매니페스트 설정에 대한 자세한 내용은 이 [문서](https://packaging.python.org/en/latest/guides/using-manifest-in/)를 참조하시오. [check-manifest](https://pypi.org/project/check-manifest/) 도구는 <code>MANIFEST.in</code>과 작업할 때도 유용할 수 있다.

### package 라이선스
다른 사용자와 package를 공유하는 경우 다른 사용자가 package를 사용할 수 있는 방법을 설명하는 라이센스를 package에 추가해야 한다. 예를 들어 reader는 [MIT 라이센스](https://mit-license.org/)로 배포된다.

라이센스는 법적 문서이므로 일반적으로 직접 작성하지 않는 것이 좋다. 대신 이미 사용 가능한 [여러](https://choosealicense.com/appendix/) 라이센스 중 하나를 [선택](https://choosealicense.com/)해야 한다.

선택한 라이센스의 텍스트가 포함된 프로젝트에 <code>LICENSE</code>라는 파일을 추가해야 한다. 그런 다음 이 파일을 <code>pyproject.toml</code>에서 참조하면 라이센스를 PyPI에 표시할 수 있다.

### package 로컬에 설치
package에 필요한 설정 및 구성을 모두 완료했다. [다음](#publish-package-PyPI) 절에서는 PyPI에서 package를 가져오는 방법에 대해 알아본다. 먼저 **편집 가능한 설치**에 대해 알아보자. 이 방법은 <code>pip</code>를 사용하여 package를 설치한 후 코드를 편집할 수 있도록 로컬에 package를 설치하는 방법이다.

> **Note**: 일반적으로 <code>pip</code>은 [<code>site-packages/</code> 폴더](https://realpython.com/python-virtual-environments-a-primer/#why-do-you-need-virtual-environments)에 package를 배치하는 **고정 설치**를 수행한다. 로컬 프로젝트를 설치하면 소스 코드가 <code>site-packages/</code>에 복사된다. 이 경우 나중에 변경한 내용이 적용되지 않는다. 먼저 package를 다시 설치해야 한다.

> 개발 중에 이는 비효율적이면서도 불만스러울 수 있다. 편집 가능한 설치는 소스 코드에 직접 연결하여 이 문제를 해결하여야 한다.

편집 가능한 설치를 [PEP 660](https://peps.python.org/pep-0660/)에서 공식화하였다. 이 기능은 package를 개발할 때 유용하다. package를 다시 설치하지 않고도 package의 모든 기능을 테스트하고 소스 코드를 업데이트할 수 있다.

<code>-e</code> 또는 <code>--editable</code> 플래그를 추가하여 <code>pip</code>를 사용하여 package를 편집 가능한 모드로 설치할 수 있다.

```shell
(venv) $ python -m pip install -e .
```

명령의 끝에 마침표(<code>.</code>)를 추가하였다. 명령에 필요한 부분이며 현재 워킹 디렉터리에 package를 설치하려고 함을 <code>pip</code>에 알린다. 일반적으로 이 경로는 <code>pyproject.toml</code> 파일에 명시된 디렉터리 경로여야 한다.

> **Note**: "*프로젝트 파일에 'pyproject.toml'이 있고 빌드 백엔드에 'build_editable' 후크가 없다.*"라는 오류 메시지가 출력될 수 있다. 이는 [PEP 660](https://github.com/pypa/setuptools/issues/2816)에 대한 Setuptools 지원의 [제한](https://github.com/pypa/setuptools/issues/2816) 때문이다. 다음 내용이 포함된 <code>setup.py</code> 파일을 추가하여 이 문제를 해결할 수 있다.

```python
# setup.py

from setuptools import setup

setup()
```

> 이는 PEP 660에 대한 기본 지원이 제공될 때까지 Setuptools의 레거시 메커니즘에 편집 가능한 설치를 위임하는 것이다.

프로젝트를 성공적으로 설치하면 현재 디렉터리에 상관없이 사용자 환경에서 사용할 수 있다. 또한 스크립트를 실행할 수 있도록 스크립트가 설정된다. realpython라는 스크립트로 reader를 정의했다는 것을 기억하자.

```shell
(venv) $ realpython
The latest tutorials from Real Python (https://realpython.com/)
  0 How to Publish an Open-Source Python Package to PyPI
  [...]
```

또한 모든 디렉터리에서 <code>python -m reader</code>를 사용하거나 REPL 또는 다른 스크립트에서 package를 import할 수 있다.

```python
>>> from reader import feed
>>> feed.get_titles()
['How to Publish an Open-Source Python Package to PyPI', ...]
```

개발 중에 package를 편집 가능한 모드로 설치하면 개발 환경이 훨씬 더 쾌적해진다. 또한 현재 워킹 디렉터리에서 사용 가능한 파일에 무의식적으로 종속될 수 있는 특정 버그를 찾는 좋은 방법이다.

시간이 좀 걸리긴 했지만, 이렇게 하면 package에 대해 해야 할 준비가 마무리되었다. 다음 절에서는 실제로 퍼블리싱하는 방법에 대해 알아보자!

## PyPI에 package 퍼블리시 <a id="publish-package-PyPI"></a>
package가 마침내 컴퓨터 외부 세계를 만날 준비가 되었다! 이 절에서는 package를 빌드하고 PyPI에 업로드하는 방법에 대해 알아본다.

PyPI에 대한 계정이 아직 없다면 지금이 [PyPI에 계정을 등록](https://pypi.org/account/register/)할 때이다. 그 동안 [TestPyPI에 계정을 등록](https://test.pypi.org/account/register/)해야 한다. TestPyPI는 매우 유용하다! 잘못하면 결과 없이 package를 퍼블리시하는 모든 단계를 시도할 수 있다.

package를 빌드하고 PyPI에 업로드하려면 [Build](https://pypa-build.readthedocs.io/)와 [Twine](https://twine.readthedocs.io/)이라는 두 도구를 사용한다. 평소처럼 <code>pip</code>를 사용하여 설치할 수 있다.

```shell
(venv) $ python -m pip install build twine
```
다음 소절에서 이러한 도구의 사용법을 설명한다.

### package 빌드
PyPI의 package는 일반 소스 코드로 배포되지 않는다. 대신 배포 package로 포장한다. 배포 package의 가장 일반적인 형식은 소스 아카이브와 [Python wheels](https://realpython.com/python-wheels/)이다.

> **Note**: Wheels는 치즈 가게에서 가장 중요한 품목인 **cheese wheels**를 지칭하여 명명되었다.

소스 아카이브는 소스 코드와 하나의 [tar](https://en.wikipedia.org/wiki/Tar_%28computing%29) 파일로 묶인 모든 지원 파일로 구성된다. 마찬가지로 wheel은 기본적으로 코드가 들어 있는 zip 아카이브이다. package에 대한 소스 아카이브와 wheel을 모두 제공해야 합니다. 일반적으로 최종 사용자에게 wheel이 더 빠르고 편리하며 소스 아카이브는 유연한 백업 방법을 제공한다.

package에 대한 소스 아카이브 및 wheel을 생성하려면 빌드를 사용한다.

```shell
(venv) $ python -m build
[...]
Successfully built realpython-reader-1.0.0.tar.gz and
    realpython_reader-1.0.0-py3-none-any.whl
```

출력의 마지막 행에서 알 수 있듯이 소스 아카이브와 휠이 생성된다. 새로 생성된 <code>dist</code> 디렉토리에서 그 결과를 찾을 수 있다.

```
realpython-reader/
│
└── dist/
    ├── realpython_reader-1.0.0-py3-none-any.whl
    └── realpython-reader-1.0.0.tar.gz
```

<code>.tar.gz</code> 파일은 원본 아카이브이고 <code>.whl</code> 파일은 wheel이다. PyPI에 업로드할 파일이며 나중에 package를 설치할 때 해당 pip이 다운로드한다.

### package 빌드 확인
새로 만든 배포 package를 업로드하기 전에 package에 원하는 파일이 포함되어 있는지 확인해야 한다. wheel 파일은 다른 확장자를 가진 진정한 [ZIP 파일](https://realpython.com/python-zipfile/)이다. 다음과 같이 압축을 풀고 내용을 검사할 수 있다.

Window: 

```shell
(venv) PS> cd .\dist
(venv) PS> Copy-Item .\realpython_reader-1.0.0-py3-none-any.whl reader-whl.zip
(venv) PS> Expand-Archive reader-whl.zip
(venv) PS> tree .\reader-whl\ /F
C:\REALPYTHON-READER\DIST\READER-WHL
├───reader
│       config.toml
│       feed.py
│       viewer.py
│       __init__.py
│       __main__.py
│
└───realpython_reader-1.0.0.dist-info
        entry_points.txt
        LICENSE
        METADATA
        RECORD
        top_level.txt
        WHEEL
```

Linux 또는 macOS

```shell
(venv) $ cd dist/
(venv) $ unzip realpython_reader-1.0.0-py3-none-any.whl -d reader-whl
(venv) $ tree reader-whl/
reader-whl/
├── reader
│   ├── config.toml
│   ├── feed.py
│   ├── __init__.py
│   ├── __main__.py
│   └── viewer.py
└── realpython_reader-1.0.0.dist-info
    ├── entry_points.txt
    ├── LICENSE
    ├── METADATA
    ├── RECORD
    ├── top_level.txt
    └── WHEEL

2 directories, 11 files
```

먼저 wheel 파일의 이름을 확장하기 위해 확장명이 <code>.zip</code>이 되도록 이름을 변경한다.

모든 소스 코드와 <code>pyproject.toml</code>가 제공한 정보가 포함된 몇 개의 새 파일이 나열된다. 특히 모든 하위 package와 <code>config.toml</code>과 같은 지원 파일이 포함되어 있는지 확인하여야 한다.

[tar ball](https://en.wikipedia.org/wiki/Tar_(computing))로 패키징되어 있기 때문에 소스 아카이브 내부도 볼 수 있다. 그러나 wheel에 예상 파일이 포함되어 있으면 원본 아카이브도 문제가 없습니다.

또한 Twine은 package 설명이 PyPI에서 올바르게 렌더링되는지 [확인](https://twine.readthedocs.io/en/stable/#twine-check)할 수 있다. <code>dist/</code>에 생성된 파일에 대해 twine 검사를 실행할 수 있다.

```shell
(venv) $ twine check dist/*
Checking distribution dist/realpython_reader-1.0.0-py3-none-any.whl: Passed
Checking distribution dist/realpython-reader-1.0.0.tar.gz: Passed
```

이것이 여러분이 직면할 수 있는 모든 문제를 해결하지는 못하지만, 좋은 방어선이 될 수 있다.

### package 업로드
이제 실제로 PyPI에 package를 업로드할 준비가 되었다. 이렇게 하려면 Twine 도구를 다시 사용하여 작성한 배포 package를 업로드하도록 명령한다.

먼저 [TestPyPI](https://packaging.python.org/guides/using-testpypi/)에 업로드하여 모든 것이 예상대로 작동하는지 확인하여야 한다.

```shell
(venv) $ twine upload -r testpypi dist/*
```

Twine이 사용자 이름과 암호를 요청한다.

> **Note**: reader package를 예로 튜토리얼을 수행한 경우 realpython-Reader 프로젝트를 업로드할 수 없다는 메시지를 출력하며 이전 명령이 실패할 것이다.

> <code>pyproject.toml</code>에 있는 이름을 고유한 이름으로 변경할 수 있다 (예: <code>test-<your-username\></code>). 그런 다음 프로젝트를 다시 빌드하고 새로 만든 파일을 TestPy에 업로드한다.

업로드가 성공하면 [TestPyPI](https://test.pypi.org/)로 빠르게 이동하고 아래로 스크롤하여 새 릴리스에서 자랑스럽게 표시되는 프로젝트를 볼 수 있다! package를 클릭하고 모든 것이 정상인지 확인한다.

reader package를 사용하여 진행한 경우 튜토리얼은 여기서 끝난다! TestPyPI로 당신이 원하는 만큼 작업할 수 있는 동안 테스트를 위해 PyPI에 샘플 package를 업로드하지 말아야 합다.

> **Note**: TestPyPI는 package가 정확히 업로드되고 프로젝트 페이지가 의도한 대로 표시되는지 확인하는 데 유용하다. 또한 TestPyPI에서 package 설치를 시도할 수 있다.

```shell
(venv) $ python -m pip install -i https://test.pypi.org/simple realpython-reader
```

> 그러나 일부 종속성을 TestPyPI에서 사용할 수 없기 때문에 이 작업이 실패할 수 있다. 이것이 문제가 아니야. PyPI에 업로드해도 package는 계속 작동한다.

만약 퍼블리싱할 자신만의 package가 있다면, 드디어 때가 왔다! 모든 준비를 마친 상태에서 이 마지막 단계는 짧다.

```shell
(venv) $ twine upload dist/*
```

요청 시 사용자 이름과 암호를 입력한다. 끝! 

[PyPI](https://pypi.org/)로 가서 업로드란 package를 찾아보자. package를 [검색](https://pypi.org/search/)하거나, [프로젝트 페이지](https://pypi.org/manage/projects/)를 보거나, 프로젝트의 URL [pypi.org/project/your-package-name/](https://pypi.org/project/realpython-reader/)로 직접 이동하여 찾을 수 있다.

축하합니다! package는 PyPI에 퍼블리시되었다.

### package 설치
이제 잠시 시간을 내어 PyPI 웹 페이지의 푸른 빛을 즐기고 친구들에게 자랑하세요.

그런 다음 터미널을 다시 돌아온다. 좋은 보상이 하나 더 있다!

pip를 사용하여 PyPI에 업로드된 package를 설치할 수 있다. 먼저 새 가상 환경을 생성하고 활성화시킨다. 그리고 다음 명령을 실행한다.

```shell
(venv) $ python -m pip install your-package-name
```

package 이름을 package에 대해 선택한 이름으로 변경한다. 예를 들어, reader package를 설치하려면 다음같이 수행한다.

```shell
(venv) $ python -m pip install realpython-reader
```

다른 서드 파티 라이브러리와 마찬가지로 pip으로 설치된 여러분의 코드를 보는 것은 멋진 기분일 것이다!

## 다른 빌드 시스탬 <a id="other-build-system"></a>

**to be filled**

### Flit - **to be filled**

### Peotry - **to be filled**

## 끝내며 <a id="concluding-remarks"></a>
이제 프로젝트를 준비하고 PyPI에 업로드하여 다른 사람이 설치하고 사용할 수 있도록 하는 방법을 설명하였다. 몇 가지 단계를 거쳐야 하지만 PyPI에서 자신의 package를 보는 것은 큰 결실이다. 여러분의 프로젝트가 유용하다는 것을 다른 사람들이 인정 하는 것은 훨씬 더 바람직할 것이다!

이 페이지에서는 다음과 같이 package를 퍼블리시하는 방법을 설명하였다.

- package의 **적합한 이름** 찾기
- <code>pyproject.toml</code>을 사용하여 package **구성**
- package **Build**
- PyPI에 package **업로드**

또한 툴과 프로세스를 표준화하기 위한 Python packaging 커뮤니티의 이니셔티브에 대해서도 설명하였다.

[Python Packaging User Guide](https://packaging.python.org/)에는 이 페이지에서 설명한 것보다 더 자세한 정보가 많이 있으니 참고하세요.

> **Note**: **소스 코드 가져오기** - 이 페이지에서 사용한 Real Python Feed Reader의 소스 코드에 접근하려면 [여기](https://realpython.com/bonus/pypi-publish-python-package-source-code/)를 클릭하세요.
