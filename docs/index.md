이 페이지는 [How to Publish an Open-Source Python Package to PyPI](https://realpython.com/pypi-publish-python-package/)를 편역한 것이다.

### 목차

- [Python Packing 살펴보기](guide/#get-to-know)
- [작은 Python Package 생성](guide/#cretae-small-python-package)
- [퍼블리쉬를 package 준비](guide/#prepare-package-publication)
- [PyPI에 package 퍼블리시](guide/#publish-package-PyPI)
- [다른 빌드 시스탬](guide/#other-build-system)
- [끝내며](guide/#concluding-remarks)

Python은 [배터리가 포함된 것](https://docs.python.org/tutorial/stdlib.html#batteries-included)으로 유명하며 [표준 라이브러리](https://docs.python.org/3/library/)에서 많은 정교한 기능을 사용할 수 있다. 그러나 언어의 잠재력을 최대한 활용하려면 [PyPI](https://pypi.org/)의 커뮤니티 **Python Packaging Index**에 기여하는 것이 바람직하다.

PyPI는 일반적으로 파이피아이(pie-pee-eye)로 발음하며 수십만 개의 package를 포함하는 저장소이다. 이것들은 사소한 [Hello, World](https://pypi.org/search/?q=hello+world)의 구현에서부터 고급 [딥 러닝 라이브러리](https://pypi.org/project/keras/)에 이르기까지 다양하다. 이 튜토리얼 페이지는 **자신의 package를 PyPI에 업로드하는 방법**을 설명하고 있다. 프로젝트를 퍼블리시(publish)하는 것이 이전보다 더 쉬워졌다. 그러나 몇 가지 단계를 거쳐야 한다.

**이 튜토리얼 페이지에서는 다음과 같은 것을 설명한다**.

- 퍼브리시할 Python package **준비**
- package **버전** 관리
- package를 **빌드**하여 PyPI에 **업로드**
- 다양한 **빌드 시스템** 이해 및 사용

이 튜토리얼에서는 예제 프로젝트인 reader package를 사용하여 콘솔에서 Real Python 튜토리얼을 읽을 수 있다. 이 package를 퍼블리시하는 방법에 대해 자세히 설명하기 전에 프로젝트에 대한 간단한 소개를 한다. reader의 전체 소스 코드가 포함된 [GitHub 저장소](https://realpython.com/bonus/pypi-publish-python-package-source-code/)를 액세스하려면 GitHub 저장소를 클릭하시오.
